//: A UIKit based Playground to present user interface
import UIKit
//import PlaygroundSupport
//
//class myViewController : UIViewController {
//
//    override func loadView() {
//        let view = UIView()
//
//        let label = UILabel()
//        label.text = "Hello World!"
//        label.textColor = .white
//
//        view.addSubview(label)
//        self.view = view
//    }
//
//}
//
//PlaygroundPage.current.liveView = myViewController()

//SE-0163
let galaxy = "Milky Way 🐮"
for char in galaxy {
    print(char)
}
galaxy.count
galaxy.isEmpty
galaxy.dropFirst()
String(galaxy.reversed())

// none ASCII
galaxy.filter { char in
    let isASCII = char.unicodeScalars.reduce(true, {
        print("here $0: \($0) $1: \($1) $1.isASCII: \($1.isASCII) result: \($0 && $1.isASCII)")
        return $0 && $1.isASCII
    })
    return isASCII
}

let endIndex = galaxy.index(galaxy.startIndex, offsetBy: 3)
//galaxy.startIndex...endIndex -> ClosedRange 神奇
var milkSubstring = galaxy[galaxy.startIndex...endIndex]
type(of: milkSubstring)

// Concatenate a String onto a Substring
milkSubstring += "🥛"     // "Milk🥛"

// create a String from a substring
let milkString = String(milkSubstring)

"👩‍💻".count // Now: 1, Before: 2
"👍🏽".count // Now: 1, Before: 2
"👨‍❤️‍💋‍👨".count // Now: 1, Before, 4

// Dictionary and Set

let nearestStarNames =  ["Proxima Centauri", "Alpha Centauri A", "Alpha Centauri B", "Barnard's Star", "Wolf 359"]
let nearestStarDistances = [4.24, 4.37, 4.37, 5.96, 7.78]
let starDistanceDict = Dictionary(uniqueKeysWithValues: zip(nearestStarNames, nearestStarDistances))

let favoriteStarVotes =  ["Alpha Centauri A", "Wolf 359", "Alpha Centauri A", "Barnard's Star"]
let mergedKeyAndValues = Dictionary(zip(favoriteStarVotes, repeatElement(1, count: favoriteStarVotes.count)), uniquingKeysWith: +)

let closeStars = starDistanceDict.filter { $0.value < 5.0 }
closeStars
let mappedCloseStars = closeStars.mapValues { "\($0)" }
mappedCloseStars

let siriusDistance = mappedCloseStars["Wolf 359", default: "unknown"]
var starWordsCount: [String: Int] = [:]
for starName in nearestStarNames {
    let numWords = starName.split(separator: " ").count
    starWordsCount[starName, default: 0] += numWords // Amazing
//    starWordsCount[starName] = numWords
}
starWordsCount

let starsByFirstLetter = Dictionary(grouping: nearestStarNames) { $0.first! }
starsByFirstLetter

starWordsCount.capacity
starWordsCount.reserveCapacity(20)
starWordsCount.capacity

// private
struct SpaceCraft  {
    private let warpCode: String
    
    init(warpCode: String){
        self.warpCode = warpCode
    }
}

extension SpaceCraft {
    func goToWarpSpeed(warpCode: String){
        if warpCode == self.warpCode {
            print("do it scooty!")
        }
    }
}

let enterprise = SpaceCraft(warpCode: "KirkIsCool")
enterprise.goToWarpSpeed(warpCode: "KirkIsCool")

// codable
struct CuriosityLog: Codable {
    enum Discovery: String, Codable {
        case rock, water, martian
    }
    
    var sol: Int
    var discoveries: [Discovery]
}

// Create a log entry for Mars sol 42
let logSol42 = CuriosityLog(sol: 42, discoveries: [.rock, .rock, .rock, .rock])

let jsonEncoder = JSONEncoder() // One currently available encoder
// Encode the data
let jsonData = try jsonEncoder.encode(logSol42)
// Create a String from the data
let jsonString = String(data: jsonData, encoding: .utf8) // "{"sol":42,"discoveries":["rock","rock","rock","rock"]}"

let jsonDecoder = JSONDecoder() // Pair decoder to JSONEncoder
// Attempt to decode the data to a CuriosityLog object
let decodedLog = try jsonDecoder.decode(CuriosityLog.self, from: jsonData)
decodedLog.sol         // 42
decodedLog.discoveries // [rock, rock, rock, rock]


// key-value coding
struct Lightsaber {
    enum Color {
        case blue, green, red
    }
    let color: Color
}
class ForceUser {
    var name: String
    var lightsaber: Lightsaber
    var master: ForceUser?
    init(name: String, lightsaber: Lightsaber, master: ForceUser? = nil) {
        self.name = name
        self.lightsaber = lightsaber
        self.master = master
    }
}

let sidious = ForceUser(name: "Darth Sidious", lightsaber: Lightsaber(color: .red))
let obiwan  = ForceUser(name: "Obi-Wan Kenobi", lightsaber: Lightsaber(color: .blue))
let anakin  = ForceUser(name: "Anakin Skywalker", lightsaber: Lightsaber(color: .blue), master: obiwan)

let nameKeyPath = \ForceUser.name
let obiwanName  = obiwan[keyPath: nameKeyPath]

// Use keypath directly inline and to drill down to sub objects
let anakinSaberColor = anakin[keyPath: \ForceUser.lightsaber.color]  // blue

// Access a property on the object returned by key path
let masterKeyPath    = \ForceUser.master
let anakinMasterName = anakin[keyPath: masterKeyPath]?.name  // "Obi-Wan Kenobi"

// Change Anakin to the dark side using key path as a setter
anakin[keyPath: masterKeyPath] = sidious
anakin.master?.name // Darth Sidious
